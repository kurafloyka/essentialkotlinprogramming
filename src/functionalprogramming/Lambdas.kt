package functionalprogramming

fun main(args: Array<String>) {
    // { parametreler -> fonksiyon gövdesi }
    // { a: Int, b: Int -> a * b }
    // { println() }

    hello()
    hello2()

    topla(3,5)
    topla2(3,5)

    max(10,20)
    max2(10,20)

    var myLambda = { ad: String, soyad: String ->
        println("Ad: $ad Soyad: $soyad")
    }

    /* Yukardaki ile aynı
    var myLambda: (String, String) -> Unit = { ad, soyad ->
        println("Ad: $ad Soyad: $soyad")
    }
    */
    myLambda("Emre", "Köse")

    var list = listOf(1,2,3,4,5)
    list.forEach { i -> println(i) }
    list.forEach {  println(it) } // tek parametre alan lambda fonk. için geçerli


}

fun hello(): Unit {
    println("hello")
}
var hello2 = { println("hello") }

fun topla(a: Int, b: Int): Int = a + b
val topla2 = { a: Int, b: Int -> a + b }

fun max(a: Int, b: Int): Int = if (a > b) a else b
var max2 = { a: Int, b: Int -> if (a > b) a else b }












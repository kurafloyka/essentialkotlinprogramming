package helloworld

import java.util.HashSet

fun main(args: Array<String>) {


    /*  var array = Array<Int>(5) { 0 }
      var array2 = arrayOf(1, 2, 3)
      for (item in array2) println(item)


      var list = listOf<String>("Kotlin", "Java", "C#", "JavaScript", "Ruby")

      for (item in 0..list.size - 1)
          println(list[item])*/

/*
    var list = mutableListOf<String>("Kotlin", "Java", "C#", "JavaScript", "Ruby")
    var list2 = arrayListOf<String>()
    var list3 = ArrayList<String>()

    for (item in list) println(list)
    list.add("C")
    list.remove("Java")

    for (item in list) println(list)

    list.removeAt(1)*/


    var map = mapOf<Int, String>(12 to "faruk", 312 to "Farih")  //sabit eleman immutable

    for (anahtar in map.keys) println("Anahtar : $anahtar Deger : ${map[anahtar]}")

    var map2 = HashMap<Int, String>()//dinamik mutable
    map2.put(34, "dasadad")
    map2.put(3, "dasadada")
    for (anahtar in map2.keys) println("Anahtar : $anahtar Deger : ${map2[anahtar]}")
    map2.replace(3, "faruk")
    for (anahtar in map2.keys) println("Anahtar : $anahtar Deger : ${map2[anahtar]}")

    var map4 = mutableMapOf<Int, String>()//Sirali olarak iletiyor


    var set = setOf<Int>(3, 23, 66, 23, 324)

    var set4 = mutableSetOf<Int>(4, 543, 35, 453, 2, 8, 35)
    set4.add(34)

    for (item in set4) println(item)

    var set5 = hashSetOf<Int>(24, 243, 23, 776, 4, 3243, 89)
}
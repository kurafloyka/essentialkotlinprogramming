package helloworld

fun main(args: Array<String>) {

    println(getMessage("Nasilsin???"))

    println(getName("faruk", "akyol"))

    fun ets() = myFunc()

    println(10.triple())
    println("Kotlin".reversed())


    var list = arrayListOf<String>("Kotlin", "Java", "C#", "JavaScript")
    for (item in list) println(item)
    list.swap(0, 3)
    println()
    for (item in list) println(item)


    5.downTo(1)  //infix func
    5 downTo 1  //extension function


    println(4 topla 5)

}

fun getMessage(message: String): String {
    println("Fonksiyonun icerisine girildi....")
    return "Message Dondu...."
}

fun getName(name: String, surName: String): String = "Adiniz : $name Soyadiniz : $surName"


fun myFunc(): Unit = println("Fonksiyon calisti")


fun Int.triple() = this * 3


fun ArrayList<String>.swap(x: Int, y: Int) {
    val temp = this[x]
    this[x] = this[y]
    this[y] = temp
}

infix fun Int.topla(sayi: Int): Int = this + sayi
package helloworld

fun main(args: Array<String>) {
    println("Hello World")


//variables
    var isim = "faruk"
    println(isim)
    isim = "elif"
    println(isim)

    val PI = 3.14
    println(PI)

//primitif degiskenler

    var soyIsim = "Emre"
    //var sayi: Byte =127
    //var sayi:Short =32767
    //var sayi: Int = 100000
    //var sayi: Long = 100L
    //var sayi : Double= 994.5
    //var sayi: Float =994.5F
    //var karakter : Char='F'
    //var bool:Boolean=false

    //referance degiskenler
    var isimT: String = "AHMET"
    var age: String = "45"


    //null deger atama
    var a: String? = null
    println(a?.length)


    var dersAdi: String = "Kotlin"
    print("$dersAdi Egitimi ${dersAdi.length}")

    var b = 20
    var c = 30

    var max = if (b > c) {
        b
    } else {
        c
    }
    println(max)

//switch case kullanimi
    var deger = 1
    when (deger) {

        1 -> {
            println("deger 1")
            println("deger 1 ile beraber")
        }
        2 -> println("deger 2")
        3, 4 -> println("deger 3")
        else -> print("bilinmeyen deger")
    }


    var deger2 = 12
    when (deger2) {


        in 1..12 -> println("1 ile 12 arasinda")
        !in 1..12 -> println("1 ile 12 arasinda degil")
    }
}
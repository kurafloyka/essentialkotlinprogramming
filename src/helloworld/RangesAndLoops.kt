package helloworld

import kotlin.random.Random

fun main(args: Array<String>) {


    1..5
    6..20
    1.rangeTo(30)
    5.downTo(1)
    5 downTo 1
    10 downTo 1 step 2


    //for (i in 1..5) println("Faruk")
    //for (i in 5.downTo(1)) println("$i : faruk")

    //for (i in 5.rangeTo(13) step 2) println("$i : faruk")


    //((0..10) step 2).forEach({ i -> println("$i : faruk") })

    /*var i = 1;
    while (i <= 5) {
        println("faruk")
        i++
    }*/

    /*var i = 1
    do {
        println("$i : faruk")
        i++
    } while (i < 11)*/

/*
    var name: String
    var password: String
    do {

        println("Enter name : ")
        name = readLine().toString()
        println("Enter password : ")
        password = readLine().toString()

    } while (name != "kotlin" && password != "123")

    println("Name : $name Password : $password")*/


    /*  for (i in 1..5) {
          if (i == 4) {
              break
          }
          println(i)
      }*/

    /*   var sayi: Int
       var toplam = 0

       while (true) {
           println()
           sayi = readLine()!!.toInt()

           if (sayi == 0) break
           toplam += sayi

       }

       println(toplam)*/


    /*for (i in 1..5) {
        println("Dongu basi")
        if (i > 1 && i < 5) continue
        println("Dongu sonu")
    }*/


    /*  disdongu@ for (i in 1..3) {
          icdongu@ for (j in 1..5)
              println("i : $i j : $j")
          if (i == 2) break@disdongu
      }*/


    /*disdongu@ for (i in 1..4) {
        icdongu@ for (j in 1..5) {
            println("i : $i j : $j")
            if (i == 2) {
                continue@disdongu
            }
        }
    }*/

    for (i in 1..5) {
        var rastgele = Random
        var sayi = rastgele.nextInt(5) + 1
        println(sayi)

    }

}
package kalitim

interface InterfaceName {
    //properties
    //methods
}

interface MyInterface {
    var prop: String
    fun foo()
    fun hello() = "Hello World"
}

interface A {
    fun aFunc()
}


class InterfaceImpl : MyInterface, A {
    override var prop: String = "kotlin"

    override fun foo() {
        TODO("Not yet implemented")
    }

    override fun aFunc() {

    }
}


fun main() {

    var myInterface = InterfaceImpl()
    println("Prop : ${myInterface.prop}")
    myInterface.foo()
    myInterface.hello()
}
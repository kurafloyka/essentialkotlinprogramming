package kalitim

interface X {
    fun hello() = println("Hello from X")
}

interface Y : T {

    fun hello() = println("Hello from Y")
}

interface T {
    fun tFunc()
}

interface B {
    interface C {
        fun cFunc()
    }
}

class Z : X, Y ,B.C{
    override fun hello() {
        super<X>.hello()
        super<Y>.hello()
    }

    override fun tFunc() {
        TODO("Not yet implemented")
    }

    override fun cFunc() {
        TODO("Not yet implemented")
    }
}


fun main() {
    val z = Z()
    z.hello()
    z.tFunc()
}
package kalitim

open class Arac(var renk: String? = null, var model: Int? = null) {
    fun calisma() = println("Arac calisiyor")
    fun hizlamma() = println("Arac hizlaniyor")
    fun yavaslama() = println("Arac yavasliyor")
}

open class TekerlekliArac(var tekerlek: Boolean = true) : Arac()

class Otomobil : TekerlekliArac()

fun main() {


    var otomobil = Otomobil()
    otomobil.tekerlek
    otomobil.calisma()
}
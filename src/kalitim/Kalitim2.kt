package kalitim

open class Kisi(var name: String, var age: Int) {
    init {
        println("Name : $name")
        println("Age : $age")

    }

    class Ogretmen(name: String, age: Int) : Kisi(name, age) {
        fun dersVerme() = println("$name ders veriyor")
    }

    class Futbolcu(name: String, age: Int) : Kisi(name, age) {
        fun fotbolOynama() = println("$name futbol oyunuyor")
    }

    class IsAdami(name: String, age: Int) : Kisi(name, age) {
        fun calisma() = println("$name calisiyor")
    }
}

fun main(args: Array<String>) {
    var ogretmen = Kisi.Ogretmen("faruk", 43)
    ogretmen.dersVerme()

    var futbolcu = Kisi.Futbolcu("veli", 34)
    futbolcu.fotbolOynama()
}
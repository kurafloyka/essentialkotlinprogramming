package kalitim

open class Person() {

    open fun displayAge(age: Int) {
        println("age : $age")
    }

    open var name: String? = null
        get() = field
        set(value) {
            field = value
        }
}

open class Girl : Person() {
    override fun displayAge(age: Int) {
        println("age: ${age - 5}")
        super.displayAge(age)
    }


    override var name: String? = null
        get() = field //super.name
        set(value) {
            field = if (value == null) "No name" else value
        }
}


fun main() {

    var girl = Girl()
    girl.displayAge(34)

    var girl2 = Girl()
    girl2.name = null

    println("Name : ${girl2.name}")


}
package kalitim

/*
* 1.Soyut siniftan nesne uretulemez
* 2. Eger bir sinifin icerisinde soyut bir method varsa mutlaka o sinifta soyut olmalidir
* 3. Soyut siniflardan soyut alt siniflar turetilebilir.
* Bu durumda ust sinifin soyut metodunu override etmek zorunda degildir.
* */


abstract class Kisi2(ad: String) {
    init {
        println("Ad : $ad")
    }

    fun yasGoster(yas: Int) {
        println("Yas : $yas")
    }

    abstract fun isTanimiYaz(isTanimi: String)
}


class Ogretmen(ad: String) : Kisi2(ad) {
    override fun isTanimiYaz(isTanimi: String) {
        println(isTanimi)
    }
}

fun main() {


    val ahmet = Ogretmen("AHMET")
    ahmet.isTanimiYaz("Matematik Ogretmeni")
    ahmet.yasGoster(34)
}


abstract class Sekil(var yukseklik: Int, var genislik: Int) {
    abstract fun alanHesapla(yukseklik: Int, genislik: Int)
}

abstract class Dikdortgen(yukseklik: Int, genislik: Int) : Sekil(yukseklik, genislik)
package objectoriented

private class Person3 {

    companion object {
        var age: Int = 20
        fun callMe() = println("farukakyol")
    }

    var name: String? = null
    fun func() = println("I am  inside")
}

fun main() {
    Person3.age
    Person3.callMe()

    val person3 = Person3()
    person3.name
    person3.func()
}
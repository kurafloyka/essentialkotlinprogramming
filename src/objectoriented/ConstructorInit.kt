package objectoriented

class Person(var name: String? = null, var age: Int = 0) {


    //class Person(var name: String, var age: Int)
/*
*
* var ile okuyup yazilabiliyor
* val ile sadece okuma yapiliyor
*
*
* */
    /*constructor(name: String, age: Int) {
        this.name = name
        this.age = age
    }*/


    /*Person(name: String, age: Int) {
        var name: String? = name
        var age: Int? = age*/

}

class User(firstName: String, userAge: Int) {
    var name: String? = null
    var age: Int? = null

    init {
        name = firstName.capitalize()
        age = userAge

        println("First Name : $name")
        println("Age : $age")
    }
}

fun main() {

    var p = Person("Faruk", 28)
    var p2 = Person("Ahmet")
    var p3 = Person(age = 12)

    var user = User("Faruk", 28)

}
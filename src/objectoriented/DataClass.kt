package objectoriented

data class Person1(var name: String, var age: Int)

fun main() {
    val person = Person1("Ali", 23)
    println("name : ${person.name} age : ${person.age}")
    println("name : ${person.component1()} age : ${person.component2()}")

    val person1 = person.copy("Emre")
    println("name : ${person1.name} age : ${person1.age}")

    println(person1.toString())

    val person2 = person1.copy()



    println("person hascode : ${person.hashCode()}")
    println("person1 hascode : ${person1.hashCode()}")
    println("person2 hascode : ${person2.hashCode()}")

    if (person.equals(person1)) println("person person1 Esittir")
    else println("person person1 esit degildir")

    if (person1.equals(person2)) println("person1 person2 Esittir")
    else println("person1 person2 esit degildir")

}
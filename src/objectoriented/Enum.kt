package objectoriented

fun guneGoreHesapla(gun: Gunler, deger: Int): Int =
    when (gun) {
        Gunler.PAZARTESI -> deger * 3
        Gunler.SALI -> deger + 3
        Gunler.CARSAMBA -> deger - 32
        Gunler.PERSEMBE -> deger + 3
        Gunler.CUMA -> deger - 342
        Gunler.CUMARTESI -> deger * 4
        Gunler.PAZAR -> deger * deger
        else -> -2

    }


enum class Gunler {

    PAZARTESI, SALI, CARSAMBA, PERSEMBE, CUMA, CUMARTESI, PAZAR
}

enum class Renk(val rgb: Int) {
    KIRMIZI(0XFF0000),
    YESIL(0X00FF00),
    MOR(0X000FFF)
}

fun main() {
    println(guneGoreHesapla(Gunler.PAZAR, 3))

    println(Renk.KIRMIZI)
    println(Renk.YESIL)
    println(Renk.MOR.rgb)
}
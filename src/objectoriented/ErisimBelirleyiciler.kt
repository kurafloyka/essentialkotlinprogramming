package objectoriented

/*encapsulation
* public   ->Herkese acik ->Tum siniflar erisebilir
* pravite  -> Sadece kendi sinifindan erisim saglanabilir
* protected -> Kendi sinifi ve alt siniflarindan erisebilir
* internal -> Sadece kendi modulunden erisim
*/
fun main() {

}


class PublicClass() {
    val i = 1
    fun func() {

        var pv = PrivateClass()
        //pc.a   erisim saglanamaz
        //pc.privateFun   erisim saglanamaz

    }
}

private class PrivateClass() {
    private val a = 0
    private fun privateFun() {}
}

open class A {

    protected val b = 2

}

class B : A() {
    fun getMethod() = b
}


class C {
    fun getValue() {

        var a = A()
        //a.b  buradan a degerine erisemez

    }


    internal class InternalClass {

        var i = 5  //bu classa baska moduile icersinden erisilemez...

    }
}
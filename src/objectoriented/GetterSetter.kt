package objectoriented

class Friend {

    var name: String = "FARUK"
        get() = field
        set(value) {
            field = value
        }
}

class Mum {
    var age: Int = 0
        get() = field
        set(value) {
            field = if (value < 18)
                18 else if (value >= 18 && value <= 30) value else value - 3
        }

    var actualAge: Int = 0
}

fun main() {

    var mum = Mum()
    mum.age = 32
    mum.actualAge = 56

    println("mum age : ${mum.age} Mum Actual Age : ${mum.actualAge}")
}
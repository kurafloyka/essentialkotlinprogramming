package objectoriented

object ObjectExample {
    //property
    //method
}


object MyObject {
    var a: Int = 0
    var b: Int = 1

    fun multiple(x: Int, y: Int): Int = x * y
}

fun main() {
    var result = MyObject.multiple(3, 4)
    println("Sonuc : $result")
    MyObject.a
    MyObject.b

    println(Math.max(34, 3))


    val book = object : Book() {
        override fun writeCode() {
            println("I don't write code.I am not a programmer...")
        }
    }

    book.eat()
    book.talk()
    book.writeCode()

    println(book.javaClass) //anonim bir nesne
    val book2 = Book()
    println(book2.javaClass)
}

open class Book {
    fun eat() = println("Eating food")
    fun talk() = println("Talking people")
    open fun writeCode() = println("Writing Code")
}
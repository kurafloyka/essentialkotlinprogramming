package objectoriented

/*
*
*
*
* a++ a.inc()
* a-- a.dec()
* a+b a.plus(b)
* a-b a.minus(b)
* a*b a.times(b)
* a/b a.div(b)
* a%b a.rem(b) ,a.mod(b)
* a..b a.rangeTo(b)
*
*
*
* */


fun main() {

    var point =Point(3,4)
    ++point
    println("point : ${point.x}, ${point.y}")

}

class Point(var x:Int=0,var y:Int=6){
    operator fun inc()=Point(++x,++y)
}
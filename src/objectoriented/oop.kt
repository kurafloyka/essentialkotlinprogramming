package objectoriented

class ClassName {

    //properties
    //methods
}

class Lamp {
    var isOn: Boolean = false
    fun turnOn() {
        isOn = true
    }

    fun turnOff() {
        isOn = false
    }
}


fun main() {
    println("faruk akyol")

    var lamp = Lamp()

    lamp.turnOff()
    lamp.turnOn()

    var araba = Araba()

    araba.model = 2017
    araba.renk = "Blue"
    araba.calisma()
    araba.hizlanma()
}
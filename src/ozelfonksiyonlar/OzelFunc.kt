package ozelfonksiyonlar

import java.nio.file.Paths

fun main(args: Array<String>) {
    /* LET
    //let fonksiyonu bir değişkenin null olup olmadığının kontrolünü yapar.

    var str: String? = "kotlin"
    if (str != null) {
        // null olmadığında çalışacak kodlar
    }else {
        // null olduğunda çalışacak kodlar
    }

    str?.let {
        // null olmadığında çalışacak kodlar
        println("null değil")
    } ?: println("null") // null olduğunda çalışacak kodlar

    val result = str?.let {
        println(it) // Argument
        5 // return value
    }

    println(result)
    */

    /*
    val task = Runnable { println("running") }
    val thread = Thread(task)
    thread.isDaemon = true
    thread.name = "thread"
    thread.state
    thread.start()

    val task2 = Runnable { println("running 2") }
    val thread2 = Thread(task2)
    thread2.apply {
        isDaemon = true
        name = "thread2"
        state
        start()
    }
    */

    // APPLY
    var dev = Developer()
    dev.age = 22
    dev.name = "John"
    dev.writeCode()

    dev.apply {
        age = 24
        name = "Lyla"
        writeCode()
    }

    // REPEAT
    repeat(10, { println("kotlin") })

}

class Developer {
    var name: String? = null
    var age: Int? = null

    fun writeCode() = println("writing code")
}
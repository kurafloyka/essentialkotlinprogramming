package ozelfonksiyonlar


fun main(args: Array<String>) {

    /* WITH
    var person = Person()

    with(person) {
        name = "Emre"
        age = 28
        run()
        eat()
    }

    person.apply {
        name = "Emre"
        age = 12
        run()
        eat()
    }
    */

    /* RUN : let ve with metodlarının birleşimidir
    val outputPath = Paths.get("/user/home").run {
        val path = resolve("output")
        path.toFile().createNewFile()
        path
    }
    */

    /* ALSO
    val adress = Address("new street", 12455)
    val user = User("Edmund", adress)

    val result = user.also { it.age = 50 }
    */

    // TAKEIF : Şart doğruysa nesneyi döndürür değilse null döndürür
    var person = Person()
    var x = person.takeIf { 5 > 2 } ?: "not a person"
    println(x)

    // TAKEUNLESS : Şart doğruysa null döndürür değilse nesneyi döndürür
    var y = person.takeUnless { it is Person } ?: "a person"
    println(y)

}

class Person {
    var name: String? = null
    var age: Int = 0

    fun run() = println("running")
    fun eat() = println("eating")
}

data class Address(var street: String? = null, var code: Int? = null)
data class User(var name: String? = null, var address: Address? = null, var age: Int? = null)





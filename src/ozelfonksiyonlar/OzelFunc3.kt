package ozelfonksiyonlar

fun main(args: Array<String>) {
    var list = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
    // Filter: şartı sağlayan elemanları döndürür
    var list2 = list.filter { it > 10 }

    list2.printArray()

    println()
    // Map: tüm elemanlara bir işlem yapılmak istendiğinde kullanılır.
    var list3 = list.map { it * 2 }
    list3.printArray()

    // All: tüm elemanların şartı sağlayıp sağlamadığını kontrol eder sağlamıyorsa false sağlıyorsa true döner
    var a = list.all { it > 5 }
    println(a)

    // Any: liste içindeki herhangi bir elemanın şartı sağlayıp sağlamadığını kontrol eder sağlamıyorsa false sağlıyorsa true döner
    var b = list.any { it > 10 }
    println(b)

    // Count: şartı sağlayan eleman sayısını döndürür
    var c = list.count { it > 5 }
    println(c)

    // Find: şartı sağlayan ilk elemanı döndürür
    var d = list.find { it > 7 }
    println(d)
}

fun <T> List<T>.printArray() {
    for (items in this) println(items)
}
